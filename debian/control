Source: opensnitch
Section: python
Priority: optional
Maintainer: Chris Lamb <lamby@debian.org>
Build-Depends:
 debhelper-compat (= 11),
 dh-golang,
 go-dep,
 dh-python,
 golang-github-evilsocket-ftrace-dev,
 golang-github-fsnotify-fsnotify-dev,
 golang-github-google-gopacket-dev,
 golang-go,
 golang-google-grpc-dev,
 golang-goprotobuf-dev,
 libnetfilter-queue-dev,
 pkg-config,
 protobuf-compiler,
 python3-dev,
 pyqt5-dev-tools,
 python3-grpc-tools,
 python3-setuptools,
Standards-Version: 4.2.1
Homepage: https://opensnitch.io/
Vcs-Git: https://salsa.debian.org/lamby/pkg-opensnitch.git
Vcs-Browser: https://salsa.debian.org/lamby/pkg-opensnitch
XS-Go-Import-Path: github.com/evilsocket/opensnitch

Package: opensnitch
Architecture: any
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Built-Using: ${misc:Built-Using}
Description: Port of the Little Snitch application firewall
 OpenSnitch works on the same principles of the macOS version, being a
 host-based firewall that notifies users when local apps are attempting to
 initiate new outgoing network connections.
 .
 When this happens, OpenSnitch will display a popup, asking the user for
 instructions on how to deal with this new process.
 .
 All user decisions are saved as rules in local JSON files. Users can edit
 these rules later to fine-tune the firewall or import/export rules from/to
 other systems.

Package: opensnitch-ui
Architecture: any
Depends:
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
 python3-grpc,
 python3-grpc-tools,
 python3-pyinotify,
Description: Port of the Little Snitch application firewall (GUI)
 OpenSnitch works on the same principles of the macOS version, being a
 host-based firewall that notifies users when local apps are attempting to
 initiate new outgoing network connections.
 .
 When this happens, OpenSnitch will display a popup, asking the user for
 instructions on how to deal with this new process.
 .
 All user decisions are saved as rules in local JSON files. Users can edit
 these rules later to fine-tune the firewall or import/export rules from/to
 other systems.
 .
 This package contains the graphical user interface (GUI).
